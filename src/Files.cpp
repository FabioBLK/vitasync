#include "Files.h"

#include <string.h>
#include <psp2/io/stat.h>
#include <psp2/io/fcntl.h>
#include <psp2/io/dirent.h>

Files::Files()
{
}

Files::~Files()
{
}

bool Files::getDirData(std::string p_path, std::vector<FilesData> *p_data)
{
    SceUID dfd = sceIoDopen(&p_path[0]);

    if (dfd >= 0)
    {
        int dirValue = 0;
        do
        {
            SceIoDirent dir;
            memset(&dir, 0, sizeof(SceIoDirent));

            dirValue = sceIoDread(dfd, &dir);
            if (dirValue > 0)
            {
                FilesData data = FilesData();
                data.name = dir.d_name;
                data.isFolder = SCE_S_ISDIR(dir.d_stat.st_mode);
                data.fileSize = (int)dir.d_stat.st_size;
                data.mtime = dir.d_stat.st_mtime;
                p_data->push_back(data);
            }
        } while (dirValue > 0);

        sceIoClose(dfd);
        return true;
    }
    return false;
}

bool Files::getDirDataQueue(std::string p_path, std::queue<FilesData> *p_data)
{
    SceUID dfd = sceIoDopen(&p_path[0]);

    if (dfd >= 0)
    {
        int dirValue = 0;
        do
        {
            SceIoDirent dir;
            memset(&dir, 0, sizeof(SceIoDirent));

            dirValue = sceIoDread(dfd, &dir);
            if (dirValue > 0)
            {
                FilesData data = FilesData();
                data.name = dir.d_name;
                data.isFolder = SCE_S_ISDIR(dir.d_stat.st_mode);
                data.fileSize = (int)dir.d_stat.st_size;
                data.mtime = dir.d_stat.st_mtime;
                p_data->push(data);
            }
        } while (dirValue > 0);

        sceIoClose(dfd);
        return true;
    }
    return false;
}

bool Files::isDirValid(std::string p_path)
{
    SceUID dfd = sceIoDopen(&p_path[0]);
    if (dfd >= 0)
    {
        return true;
    }
    return false;
}

bool Files::createDir(std::string p_path)
{
    sceIoMkdir(&p_path[0u], 0777);
    return isDirValid(p_path);
}

int Files::getFileSize(std::string p_path)
{
    SceUID fileID = sceIoOpen(&p_path[0u], SCE_O_RDONLY, 0);

    if (fileID >= 0)
    {
        SceIoStat fileStatus;
        int getFileStatus = sceIoGetstat(&p_path[0u], &fileStatus);
        if (getFileStatus < 0)
        {
            return 0;
        }

        sceIoClose(fileID);

        return fileStatus.st_size;
    }

    return 0;
}

bool Files::loadFileTXTData(std::string p_path, std::string *p_data)
{
    SceUID fileID = sceIoOpen(&p_path[0u], SCE_O_RDONLY, 0);

    if (fileID >= 0)
    {
        SceIoStat fileStatus;
        int getFileStatus = sceIoGetstat(&p_path[0u], &fileStatus);
        if (getFileStatus < 0)
        {
            return false;
        }

        char *fileData = new char[fileStatus.st_size + 1];
        sceIoRead(fileID, fileData, fileStatus.st_size);
        fileData[fileStatus.st_size] = '\0';
        *p_data = fileData;
        delete[] fileData;

        sceIoClose(fileID);
        return true;
    }

    return false;
}

bool Files::loadFileData(std::string p_path, FILE *p_fp, char *p_data, int p_fileSize)
{
    p_fp = fopen(&p_path[0u], "rb");

    if (!p_fp)
    {
        return false;
    }

    if ((size_t)p_fileSize != fread(p_data, 1, (size_t)p_fileSize, p_fp))
    {
        return false;
    }

    return true;
}

int Files::saveFileData(std::string p_path, std::string p_data)
{
    SceUID fileID = sceIoOpen(&p_path[0u], SCE_O_WRONLY | SCE_O_CREAT | SCE_O_TRUNC, 0777);

    if (fileID >= 0)
    {
        int bytes = sceIoWrite(fileID, &p_data[0u], p_data.size());

        sceIoClose(fileID);

        return bytes;
    }

    return 0;
}

SceUID Files::openFileToSave(std::string p_path)
{
    SceUID fileID = sceIoOpen(&p_path[0u], SCE_O_WRONLY | SCE_O_CREAT, 0777);
    return fileID;
}

void Files::closeFile(SceUID p_id)
{
    sceIoClose(p_id);
}

int Files::appendDataToFile(std::string p_path, std::string p_data)
{
    SceUID fileID = sceIoOpen(&p_path[0u], SCE_O_WRONLY | SCE_O_CREAT | SCE_O_APPEND, 0777);

    if (fileID >= 0)
    {
        int bytes = sceIoWrite(fileID, &p_data[0u], p_data.size());

        sceIoClose(fileID);

        return bytes;
    }

    return 0;
}

size_t Files::writeCurlDataCallBack(void *ptr, size_t size, size_t nmemb, FILE *stream)
{
    size_t written = sceIoWrite(*(int *)stream, ptr, size * nmemb);
    return written;
}

size_t Files::readCurlDataCallBack(char *buffer, size_t size, size_t nitems, void *instream)
{
    size_t bytes_read;

    /* I'm doing it this way to get closer to what the reporter is doing.
     Technically we don't need to do this, we could just use the default read
     callback which is fread. Also, 'size' param is always set to 1 by libcurl
     so it's fine to pass as buffer, size, nitems, instream. */
    bytes_read = fread(buffer, 1, (size * nitems), (FILE *)instream);

    return bytes_read;
}