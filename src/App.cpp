#include "App.h"
#include "Files.h"
#include "VitaIME.h"
#include "Downloader.h"
#include "qrCode/QrCode.hpp"
#include <vector>
#include <string.h>
#include <psp2/kernel/threadmgr.h>

#include "qrCode/QrCode.hpp"
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "nanoSVG/stb_image_write.h"
#define NANOSVG_IMPLEMENTATION
#include "nanoSVG/nanosvg.h"
#define NANOSVGRAST_IMPLEMENTATION
#include "nanoSVG/nanosvgrast.h"

App::App()
{
}

App::~App()
{
}

ConfigDataJSON App::CheckConfig()
{
    Files files = Files();
    std::string configDir = "ux0:data/psvitasync";
    bool exists = files.isDirValid(configDir);
    if (!exists)
    {
        bool dirCreated = files.createDir(configDir);
    }

    std::string currentConfig = "";
    files.loadFileTXTData(configFilePath, &currentConfig);

    if (currentConfig.size() == 0)
    {
        currentConfig = tools::newConfigObject();
        files.saveFileData(configFilePath, currentConfig);
    }

    return tools::parseConfigData(currentConfig);
}

void App::SetStatus()
{
    if (configData.appKey == "NULL" || configData.appSecret == "NULL")
    {
        status = AppStatus::ConfigScreen;
    }
    else if (configData.dropboxAuth == "NULL" || configData.dropboxAuth.empty())
    {
        status = AppStatus::SyncInstructions;
    }
    else if (!configData.authorized)
    {
        status = AppStatus::Authorizing;
    }
    else
    {
        status = AppStatus::SelectTransferAction;
    }
}

void App::StartApp()
{
    configData = CheckConfig();
    int uiStart = uiManager.StartUI();

    if (uiStart < 0)
    {
        printf("Error starting ui");
        return;
    }

    SetStatus();

    while (status != AppStatus::Exit)
    {
        if (status == AppStatus::Directory || status == AppStatus::RemoteDirectory)
        {
            DirectoryView();
        }
        else if (status == AppStatus::ConfigScreen)
        {
            ConfigScreenView();
        }
        else if (status == AppStatus::SyncInstructions)
        {
            SyncInstructions();
        }
        else if (status == AppStatus::Authorizing)
        {
            AuthorizingView();
        }
        else if (status == AppStatus::FileTransferDownload || status == AppStatus::FileTransferUpload)
        {
            FileTransferView();
        }
        else if (status == AppStatus::SelectTransferAction)
        {
            SelectTransferActionView();
        }
        else if (status == AppStatus::SelectLocalFiles || status == AppStatus::SelectRemoteFiles)
        {
            DirectoryView();
        }
        else if (status == AppStatus::CheckStatus)
        {
            SetStatus();
        }
        else
        {
            status = AppStatus::Exit;
        }
    }

    uiManager.FinishUI();
}

void App::DirectoryView()
{
    int position = 0;

    Files files = Files();
    std::vector<FilesData> filesData;
    Downloader downloader = Downloader();
    bool downloadingRemoteList = false;
    bool localFilesView = (status == AppStatus::SelectLocalFiles || status == AppStatus::Directory);
    bool selectionEnabled = (status == AppStatus::SelectLocalFiles || status == AppStatus::SelectRemoteFiles);
    bool allSelected = false;
    std::string downloadStatus;
    std::queue<FileInfoJSON> fileInfoList;
    std::string pathString = (configData.syncFolder.empty() || configData.syncFolder == "NULL") ? startPath : configData.syncFolder;
    std::string alert = "";
    std::string viewInstructionsText = "Select the local files you want to upload to your Dropbox folder";

    vita2d_texture *bgImage;

    if (localFilesView)
    {
        files.getDirData(pathString, &filesData);
        bgImage = uiManager.LoadTexture("app0:assets/sdcard.png");
        if (selectionEnabled)
        {
            viewInstructionsText = "Select the local to download the previously selected files";
        }
    }
    else
    {
        pathString = "";
        downloadingRemoteList = true;
        bgImage = uiManager.LoadTexture("app0:assets/cloud.png");

        if (selectionEnabled)
        {
            viewInstructionsText = "Select the remote files you want to download to your SDCard";
        }
        else
        {
            viewInstructionsText = "Select the remote folder you want to upload the previously selected files";
        }
    }

    while (1)
    {
        if (downloadingRemoteList)
        {
            std::string remoteListoutput;
            uiManager.BeginFrame();

            uiManager.DrawTexture(bgImage, DISPLAY_WIDTH / 2 - 256, DISPLAY_HEIGTH / 2 - 256, 1);
            uiManager.DrawText(downloadStatus, 50, 25, colorBlack, 20);
            uiManager.DrawText("Downloading Remote file list. Please wait.", DISPLAY_WIDTH - 500, 25, colorBlack, 14);

            uiManager.EndFrame();

            pathString = (pathString == "/") ? "" : pathString;
            int response = downloader.DropboxRequestFileList(configData.accessToken, pathString, &remoteListoutput);
            if (response != 200)
            {
                downloadStatus = "Error downloading file list from server. Error code: " + std::to_string(response);
            }
            else
            {
                fileInfoList = tools::parseFileInfoData(remoteListoutput);
                while (!fileInfoList.empty())
                {
                    FilesData newFileData;
                    newFileData.name = fileInfoList.front().name;
                    newFileData.fileSize = fileInfoList.front().size;
                    newFileData.isFolder = fileInfoList.front().tag == "folder";
                    newFileData.selectedToTransfer = false;

                    filesData.push_back(newFileData);
                    fileInfoList.pop();
                }
            }
            downloadingRemoteList = false;
        }

        if (input.GetKeyDown(SCE_CTRL_DOWN) && buttonTime > SLEEP_TIME)
        {
            position++;
            buttonTime = 0;
        }
        else if (input.GetKeyDown(SCE_CTRL_UP) && buttonTime > SLEEP_TIME)
        {
            position--;
            buttonTime = 0;
        }
        else if (input.GetKeyDown(SCE_CTRL_CIRCLE) && buttonTime > SLEEP_TIME)
        {
            alert = "";
            if (position <= filesData.size() - 1)
            {
                if (filesData[position].isFolder)
                {
                    pathString += filesData[position].name + "/";

                    filesData.clear();
                    if (localFilesView)
                    {
                        files.getDirData(pathString, &filesData);
                    }
                    else
                    {
                        if (pathString.find_first_of('/', pathString.size()) > 0)
                        {
                            pathString = "/" + pathString;
                        }
                        downloadingRemoteList = true;
                    }

                    position = 0;
                }
            }

            buttonTime = 0;
        }
        else if (input.GetKeyDown(SCE_CTRL_CROSS) && buttonTime > SLEEP_TIME)
        {
            std::string upperFolder = pathString;
            if (upperFolder.length() > 0)
            {
                upperFolder.pop_back();
            }

            int foundPos = upperFolder.find_last_of("/");

            if (foundPos >= 0)
            {
                pathString = upperFolder.substr(0, foundPos) + "/";
                filesData.clear();
                if (localFilesView)
                {
                    files.getDirData(pathString, &filesData);
                }
                else
                {
                    downloadingRemoteList = true;
                }
                position = 0;
            }

            buttonTime = 0;
        }
        else if (input.GetKeyDown(SCE_CTRL_TRIANGLE) && buttonTime > SLEEP_TIME)
        {
            if (!filesData[position].isFolder)
            {
                bool markedToTransfer = !filesData[position].selectedToTransfer;
                filesData[position].selectedToTransfer = markedToTransfer;
            }
            buttonTime = 0;
        }
        else if (input.GetKeyDown(SCE_CTRL_SQUARE) && buttonTime > SLEEP_TIME)
        {
            allSelected = !allSelected;
            for (int i = 0; i < filesData.size(); i++)
            {
                if (!filesData[i].isFolder)
                {
                    filesData[i].selectedToTransfer = allSelected;
                }
            }
            buttonTime = 0;
        }
        else if (input.GetKeyDown(SCE_CTRL_START) && buttonTime > SLEEP_TIME)
        {
            if (status == AppStatus::Directory || status == AppStatus::SelectLocalFiles)
            {
                configData.syncFolder = pathString;

                std::string configFile = tools::getConfigString(configData);
                files.saveFileData(configFilePath, configFile);
            }

            if (status == AppStatus::SelectLocalFiles)
            {
                while (!selectedLocalFiles.empty())
                {
                    selectedLocalFiles.pop();
                }

                for (int i = 0; i < filesData.size(); i++)
                {
                    if (filesData[i].selectedToTransfer)
                    {
                        selectedLocalFiles.push(filesData[i]);
                    }
                }

                status = AppStatus::RemoteDirectory;
                break;
            }
            else if (status == AppStatus::SelectRemoteFiles)
            {
                while (!selectedLocalFiles.empty())
                {
                    selectedLocalFiles.pop();
                }

                for (int i = 0; i < filesData.size(); i++)
                {
                    if (filesData[i].selectedToTransfer)
                    {
                        selectedLocalFiles.push(filesData[i]);
                    }
                }

                remoteFolder = pathString;
                status = AppStatus::Directory;
                break;
            }
            else if (status == AppStatus::Directory)
            {
                if (pathString != rootPath)
                {
                    status = AppStatus::FileTransferDownload;
                    break;
                }
                else
                {
                    alert = "You cant select a root folder to Download";
                }
            }
            else if (status == AppStatus::RemoteDirectory)
            {
                remoteFolder = pathString;
                status = AppStatus::FileTransferUpload;
                break;
            }
            else
            {
                status == AppStatus::SelectTransferAction;
                break;
            }
        }

        if (input.GetKeyDown(SCE_CTRL_SELECT) && buttonTime > SLEEP_TIME)
        {
            status = AppStatus::Exit;
            uiManager.UnloadTexture(bgImage);
            buttonTime = 0;
            break;
        }

        uiManager.BeginFrame();

        uiManager.DrawTexture(bgImage, DISPLAY_WIDTH / 2 - 256, DISPLAY_HEIGTH / 2 - 256, 1);

        int listPosition = tools::Clamp(position, 0, filesData.size() - 1);
        position = listPosition;
        uiManager.DrawDirList(filesData, listPosition, selectionEnabled, bgColorWhiteTransparent);
        uiManager.DrawText(alert, DISPLAY_WIDTH - 250, DISPLAY_HEIGTH - 50, colorBlack, 14);
        uiManager.DrawText("", DISPLAY_WIDTH - 550, 25, colorBlack, 14);
        uiManager.DrawText("Current Path: " + pathString, 50, 30, colorBlack, 14);
        uiManager.DrawText(viewInstructionsText, 50, 15, colorBlack, 14);
        uiManager.DrawText("\"O\" - Enter folder     \"X\" - Exit folder     \"SQUARE\" - Select single file    \"TRIANGLE\" - Select all files     \"START\" - Confirm Selection and advance", 50, DISPLAY_HEIGTH - 20, colorBlack, 14);
        uiManager.DrawText(downloadStatus, DISPLAY_WIDTH - 550, DISPLAY_HEIGTH - 50, colorBlack, 20);
        uiManager.EndFrame();
        buttonTime++;
    }
}

void App::SyncInstructions()
{
    Files files = Files();
    std::string url = "https://www.dropbox.com/oauth2/authorize?client_id=" + configData.appKey + "&response_type=code";
    qrcodegen::QrCode qr0 = qrcodegen::QrCode::encodeText(&url[0u], qrcodegen::QrCode::Ecc::MEDIUM);
    std::string svg = qr0.toSvgString(4);

    files.saveFileData("ux0:data/psvitasync/svgfile.svg", svg);
    std::string pngFilePath = "ux0:data/psvitasync/pngQRCode.png";
    bool converted = convertSVGToPNG("ux0:data/psvitasync/svgfile.svg", pngFilePath);
    vita2d_texture *image = vita2d_load_PNG_file(&pngFilePath[0u]);

    VitaIME ime = VitaIME();

    std::string userInput = "NULL";
    char *cArr = new char[512];

    while (1)
    {
        uiManager.BeginFrame();

        uiManager.DrawSyncInstructionsView(url, image, bgColorWhiteTransparent, colorBlack);

        if (input.GetKeyDown(SCE_CTRL_START) && buttonTime > SLEEP_TIME)
        {
            //input.OpenInputDialog();
            buttonTime = 0;
            if (ime.ime_dialog_string(cArr, "Type the authorization code", "") != 0)
            {
                return;
            }
            userInput = cArr;
        }
        if (input.GetKeyDown(SCE_CTRL_SELECT) && buttonTime > SLEEP_TIME)
        {
            status = AppStatus::Exit;
            buttonTime = 0;
            break;
        }

        if (!userInput.empty() && userInput != "NULL")
        {
            configData.dropboxAuth = userInput;

            std::string configFile = tools::getConfigString(configData);
            files.saveFileData(configFilePath, configFile);
            status = AppStatus::Authorizing;
            delete cArr;
            uiManager.UnloadTexture(image);
            break;
        }

        uiManager.EndFrame();
        buttonTime++;
    }
}

void App::AuthorizingView()
{
    bool authorizing = false;
    Files files = Files();

    Downloader downloader = Downloader();
    std::string authResult = "";
    std::string downloadedStatus = "Waiting";

    while (1)
    {
        uiManager.BeginFrame();

        uiManager.DrawAuthorizingView(bgColorWhiteTransparent, colorBlack, configData.dropboxAuth, authResult, downloadedStatus);

        if (input.GetKeyDown(SCE_CTRL_START)&& buttonTime > SLEEP_TIME)
        {
            buttonTime = 0;
            authorizing = false;
        }

        if (input.GetKeyDown(SCE_CTRL_CROSS)&& buttonTime > SLEEP_TIME)
        {
            buttonTime = 0;
            configData.dropboxAuth = "NULL";

            std::string configFile = tools::getConfigString(configData);
            files.saveFileData(configFilePath, configFile);

            status = AppStatus::SyncInstructions;
            break;
        }

        uiManager.EndFrame();
        buttonTime++;

        if (!authorizing)
        {
            authorizing = true;
            int response = downloader.DropboxRequestToken(configData.dropboxAuth, configData.appKey, configData.appSecret, &authResult);
            if (response == 200)
            {
                if (!authResult.empty())
                {
                    AuthorizationTokenJSON authData = tools::parseAuthorizationTokenData(authResult);
                    if (!authData.access_token.empty())
                    {
                        configData.accessToken = authData.access_token;
                        configData.authorized = true;
                        downloadedStatus = "Completed";

                        std::string configFile = tools::getConfigString(configData);
                        files.saveFileData(configFilePath, configFile);
                        status = AppStatus::SelectTransferAction;
                        break;
                    }
                    else
                    {
                        downloadedStatus = "Error loading the token code. Press \"START\" to try again";
                    }
                }
                else
                {
                    downloadedStatus = "Check if your typed code is correct. Press \"START\" to try again";
                }
            }
            else
            {
                downloadedStatus = "Error Connecting to the Endpoint. Press \"START\" to try again";
            }
        }
    }
}

void App::FileTransferView()
{
    std::string output;
    std::string downloadStatus;
    std::string downloadCountStatus;
    std::string errorReport;
    int successCount = 0;
    int failureCount = 0;
    Downloader downloader = Downloader();
    std::queue<FileInfoJSON> fileInfoList;
    std::queue<FilesData> *localFiles = &selectedLocalFiles;
    Files files = Files();
    bool downloading = (status == AppStatus::FileTransferDownload);
    bool uploading = (status == AppStatus::FileTransferUpload);
    int downloadCount = 0;
    std::string typeText;

    while (1)
    {
        if (!downloading && !uploading)
        {
            uiManager.BeginFrame();
            uiManager.DrawFinishTransferView(bgColorWhiteTransparent, colorBlack, downloadStatus, errorReport, configData.syncFolder);
            uiManager.EndFrame();
        }

        if (downloading)
        {
            uiManager.BeginFrame();
            typeText = "DOWNLOAD";
            uiManager.DrawTransferProgressView(bgColorWhiteTransparent, colorBlack, typeText, downloadStatus, downloadCountStatus);

            uiManager.EndFrame();

            if (localFiles->empty())
            {
                downloadCount = 0;
                downloadStatus = "Download Complete";
                downloading = false;
            }
            else
            {
                if (!localFiles->front().isFolder)
                {
                    downloadStatus = "Downloading File " + localFiles->front().name;
                    downloadCountStatus = std::to_string(localFiles->size()) + " files left";
                    remoteFolder = (remoteFolder.empty()) ? "/" : remoteFolder;
                    int downloadResult = downloader.DropboxDownloadFile(configData.accessToken, remoteFolder + localFiles->front().name, configData.syncFolder + localFiles->front().name);
                    if (downloadResult == 200)
                    {
                        successCount++;
                    }
                    else
                    {
                        failureCount++;
                        std::string report = "\nFailed To Download file " + localFiles->front().name + " - Error code " + std::to_string(downloadResult);
                        errorReport += report;
                    }
                }
                localFiles->pop();
            }
        }

        if (uploading)
        {
            uiManager.BeginFrame();

            typeText = "UPLOAD";
            uiManager.DrawTransferProgressView(bgColorWhiteTransparent, colorBlack, typeText, downloadStatus, downloadCountStatus);

            uiManager.EndFrame();

            if (localFiles->empty())
            {
                downloadStatus = "Upload Complete";
                uploading = false;
            }
            else
            {
                if (!localFiles->front().isFolder)
                {
                    downloadStatus = "Uploading File " + localFiles->front().name;
                    downloadCountStatus = std::to_string(localFiles->size()) + " files left";
                    remoteFolder = (remoteFolder.empty()) ? "/" : remoteFolder;
                    int downloadResult = downloader.DropboxUploadFile(configData.accessToken, remoteFolder + localFiles->front().name, configData.syncFolder + localFiles->front().name);
                    if (downloadResult == 200)
                    {
                        successCount++;
                    }
                    else
                    {
                        failureCount++;
                        std::string report = "\nFailed To Upload file " + localFiles->front().name + " - Error code " + std::to_string(downloadResult);
                        errorReport += report;
                    }
                }

                localFiles->pop();
            }
        }

        if (input.GetKeyDown(SCE_CTRL_CIRCLE) && buttonTime > SLEEP_TIME)
        {
            status = AppStatus::SelectTransferAction;
            buttonTime = 0;
            break;
        }
        if (input.GetKeyDown(SCE_CTRL_SELECT) && buttonTime > SLEEP_TIME)
        {
            status = AppStatus::Exit;
            downloader.~Downloader();
            buttonTime = 0;
            break;
        }
        buttonTime++;
    }
}

void App::SelectTransferActionView()
{
    std::string downloadStatus;

    while (1)
    {
        uiManager.BeginFrame();
        uiManager.DrawSelectTransferView(bgColorWhiteTransparent, colorBlack, downloadStatus, configData.syncFolder);
        uiManager.EndFrame();

        if (input.GetKeyDown(SCE_CTRL_CIRCLE)&& buttonTime > SLEEP_TIME)
        {
            status = AppStatus::SelectRemoteFiles;
            buttonTime = 0;
            break;
        }

        if (input.GetKeyDown(SCE_CTRL_CROSS)&& buttonTime > SLEEP_TIME)
        {
            status = AppStatus::SelectLocalFiles;
            buttonTime = 0;
            break;
        }
        if (input.GetKeyDown(SCE_CTRL_LTRIGGER)&& buttonTime > SLEEP_TIME)
        {
            status = AppStatus::ConfigScreen;
            buttonTime = 0;
            break;
        }
        if (input.GetKeyDown(SCE_CTRL_SELECT)&& buttonTime > SLEEP_TIME)
        {
            status = AppStatus::Exit;
            buttonTime = 0;
            break;
        }
        buttonTime++;
    }
}

void App::ConfigScreenView()
{
    Files files = Files();
    std::string url = "https://github.com/FabioBLK/PSVitaSync";
    qrcodegen::QrCode qr0 = qrcodegen::QrCode::encodeText(&url[0u], qrcodegen::QrCode::Ecc::MEDIUM);
    std::string svg = qr0.toSvgString(4);

    files.saveFileData("ux0:data/psvitasync/svgInstructions.svg", svg);
    std::string pngFilePath = "ux0:data/psvitasync/pngQRInstructions.png";
    bool converted = convertSVGToPNG("ux0:data/psvitasync/svgInstructions.svg", pngFilePath);
    vita2d_texture *image = vita2d_load_PNG_file(&pngFilePath[0u]);

    VitaIME ime = VitaIME();

    std::string userInput = "NULL";
    char *cArr = new char[512];

    std::string appKeyLabel = "Application Key: ";
    std::string appSecretLabel = "Application Secret: ";

    std::string appKeyValue = (configData.appKey.empty() || configData.appKey == "NULL") ? "<EMPTY>" : configData.appKey;
    std::string appSecretValue = (configData.appSecret.empty() || configData.appSecret == "NULL") ? "<EMPTY>" : configData.appSecret;

    std::vector<std::string> optionsList;

    std::string warningString = "";
    optionsList.push_back(appKeyLabel + appKeyValue);
    optionsList.push_back(appSecretLabel + appSecretValue);

    int startBoxYPosition = 375;

    int position = 0;

    while (1)
    {
        position = tools::Clamp(position, 0, optionsList.size() - 1);

        uiManager.BeginFrame();
        uiManager.DrawConfigScreenView(url, bgColorWhiteTransparent, colorBlack, image, optionsList, position, startBoxYPosition);
        uiManager.DrawText(warningString, DISPLAY_WIDTH - 600, 25, colorBlack, 14);
        uiManager.EndFrame();

        if (input.GetKeyDown(SCE_CTRL_DOWN)&& buttonTime > SLEEP_TIME)
        {
            position++;
            buttonTime = 0;
        }
        else if (input.GetKeyDown(SCE_CTRL_UP)&& buttonTime > SLEEP_TIME)
        {
            position--;
            buttonTime = 0;
        }
        if (input.GetKeyDown(SCE_CTRL_CIRCLE)&& buttonTime > SLEEP_TIME)
        {
            buttonTime = 0;
            if (ime.ime_dialog_string(cArr, "Type the code", "") != 0)
            {
                return;
            }
            userInput = cArr;

            if (position == 0)
            {
                appKeyValue = userInput;
            }
            else
            {
                appSecretValue = userInput;
            }
            optionsList.clear();
            optionsList.push_back(appKeyLabel + appKeyValue);
            optionsList.push_back(appSecretLabel + appSecretValue);
        }
        if (input.GetKeyDown(SCE_CTRL_START)&& buttonTime > SLEEP_TIME)
        {
            buttonTime = 0;
            if (!appKeyValue.empty() && !appSecretValue.empty() && appKeyValue != "<EMPTY>" && appSecretValue != "<EMPTY>")
            {
                configData.appKey = appKeyValue;
                configData.appSecret = appSecretValue;

                std::string configFile = tools::getConfigString(configData);
                files.saveFileData(configFilePath, configFile);

                status = AppStatus::CheckStatus;
                break;
            }
            else
            {
                warningString = "You need to type a value in both AppKey and AppSecret";
            }
        }
        if (input.GetKeyDown(SCE_CTRL_SELECT)&& buttonTime > SLEEP_TIME)
        {
            buttonTime = 0;
            status = AppStatus::Exit;
            break;
        }
        buttonTime++;
    }
}

bool App::convertSVGToPNG(std::string p_svgFilePath, std::string p_pngSaveFilePath)
{
    NSVGimage *image = NULL;
    NSVGrasterizer *rast = NULL;

    int w, h;

    image = nsvgParseFromFile(&p_svgFilePath[0u], "px", 96.0f);
    if (image == NULL)
    {
        return false;
    }

    w = (int)image->width;
    h = (int)image->height;

    rast = nsvgCreateRasterizer();
    if (rast == NULL)
    {
        return false;
    }

    unsigned char *img = (unsigned char *)malloc(w * h * 4);
    if (img == NULL)
    {
        return false;
    }

    nsvgRasterize(rast, image, 0, 0, 1, img, w, h, w * 4);

    stbi_write_png(&p_pngSaveFilePath[0u], w, h, 4, img, w * 4);

    return true;
}
