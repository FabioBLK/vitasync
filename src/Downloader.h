#include <curl/curl.h>
#include "Files.h"
#include <functional>

class Downloader
{
private:
    CURL *curlHandle;
    Files files;
    int NetworkDownload(std::string p_httpVerb, std::string p_apiUrl, std::string p_filePath, curl_slist *p_headers, std::string p_body);
    int NetworkUpload(std::string p_httpVerb, std::string p_apiUrl, std::string p_uploadFilePath, curl_slist *p_headers);
    int NetworkDownloadToMemory(std::string p_httpVerb, std::string p_apiUrl, std::string *p_outputStream, curl_slist *p_headers, std::string p_body);
    static size_t writeCurlToStringCallBack(void *contents, size_t size, size_t nmemb, std::string *s);
    void NetInit();
    void HttpInit();
public:
    Downloader();
    ~Downloader();
    int DownloadFile(std::string p_url, std::string p_filePath);
    int DropboxDownloadFile(std::string p_userToken, std::string p_fileName, std::string p_saveFilePath);
    int DropboxUploadFile(std::string p_userToken, std::string p_fileName, std::string p_uploadFilePath);
    int DropboxRequestToken(std::string p_authCode, std::string p_appId, std::string p_appSecret, std::string *p_outputStream);
    int DropboxRequestFileList(std::string p_userToken, std::string p_subfolderPath, std::string *p_outputStream);
    int DropboxCreateFolder(std::string p_userToken, std::string p_fileName, std::string *p_outputStream);
};
