#include "mySamples.h"

#include <psp2/ctrl.h>
#include <psp2/io/stat.h>
#include <psp2/sysmodule.h>
#include <psp2/net/net.h>
#include <psp2/net/netctl.h>
#include <psp2/net/http.h>
#include <psp2/io/fcntl.h>
#include <vita2d.h>
#include <string.h>
#include <stdlib.h>
#include <string>
#include <curl/curl.h>

#include "Files.h"
#include "tools.h"
#include "Downloader.h"

#include "qrCode/QrCode.hpp"
#include <jansson.h>

namespace mySamples
{
int logIndex = 0;
int logYPos[3];

void DrawSampleScreen()
{
    SceCtrlData pad;

    vita2d_texture *image;

    vita2d_font *ttfFont;
    vita2d_font *vita2dFont[42];

    vita2d_init();
    vita2d_set_clear_color(RGBA8(0x40, 0x40, 0x40, 0xFF));

    ttfFont = vita2d_load_font_file("app0:assets/font/droidsans.ttf");

    for (int i = 0; i < 42; i++)
    {
        vita2dFont[i] = vita2d_load_font_file("app0:assets/font/whitney-book.ttf"); //droidsans.ttf
    }

    /*
        * Load the statically compiled image.png file.
        */
    //image = vita2d_load_PNG_buffer(&_binary_image_png_start);
    image = vita2d_load_PNG_file("app0:assets/image.png");

    memset(&pad, 0, sizeof(pad));

    while (1)
    {
        sceCtrlPeekBufferPositive(0, &pad, 1);

        if (pad.buttons & SCE_CTRL_START)
            break;

        vita2d_start_drawing();
        vita2d_clear_screen();

        vita2d_draw_texture(image, 0, 0);

        vita2d_draw_rectangle(20, 20, 400, 250, RGBA8(255, 0, 0, 255));
        vita2d_draw_rectangle(680, 350, 100, 150, RGBA8(0, 0, 255, 255));
        vita2d_draw_fill_circle(200, 420, 100, RGBA8(0, 255, 0, 255));

        vita2d_draw_line(500, 30, 800, 300, RGBA8(255, 0, 255, 255));

        vita2d_font_draw_text(ttfFont, 20, 20, RGBA8(255, 255, 255, 255), 20, "TTF Font Sample!");

        vita2d_font_draw_text(vita2dFont[25], 300, 355, RGBA8(255, 255, 255, 255), 25, "HEEEEEEEEEEEEERE");

        vita2d_draw_rectangle(40, 40, 100, 100, RGBA8(128, 64, 192, 255));
        vita2d_set_blend_mode_add(1);
        vita2d_draw_rectangle(40, 60, 200, 60, RGBA8(0, 100, 0, 128));
        vita2d_set_blend_mode_add(0);

        vita2d_end_drawing();
        vita2d_swap_buffers();
    }

    /*
        * vita2d_fini() waits until the GPU has finished rendering,
        * then we can free the assets freely.
        */
    vita2d_fini();
    vita2d_free_texture(image);
    vita2d_free_font(ttfFont);
}

void netInit()
{
    sceSysmoduleLoadModule(SCE_SYSMODULE_NET);

    SceNetInitParam netInitParam;
    int size = 4 * 1024 * 1024;
    netInitParam.memory = malloc(size);
    netInitParam.size = size;
    netInitParam.flags = 0;
    sceNetInit(&netInitParam);

    sceNetCtlInit();
}

void httpInit()
{
    sceSysmoduleLoadModule(SCE_SYSMODULE_HTTP);

    sceHttpInit(4 * 1024 * 1024);
}

void printLog(vita2d_font *font, const char *text, const int yPos)
{
    vita2d_font_draw_text(font, 300, yPos, RGBA8(255, 255, 255, 255), 25, text);
}

struct stringcurl
{
    char *ptr;
    size_t len;
};

void init_string(struct stringcurl *s)
{
    s->len = 0;
    s->ptr = (char *)malloc(s->len + 1);
    if (s->ptr == NULL)
    {
        fprintf(stderr, "malloc() failed\n");
        return;
        //exit(EXIT_FAILURE);
    }
    s->ptr[0] = '\0';
}

static size_t write_data_to_disk(void *ptr, size_t size, size_t nmemb, void *stream)
{
    tools::LogToFile("-----curl callback-----");
    std::string fileId = std::to_string(*(int *)stream);
    std::string data = (char *)ptr;
    tools::LogToFile(fileId);
    tools::LogToFile(data);
    size_t written = sceIoWrite(*(int *)stream, ptr, size * nmemb);
    return written;
}

size_t writefunc(void *ptr, size_t size, size_t nmemb, struct stringcurl *s)
{
    size_t new_len = s->len + size * nmemb;
    s->ptr = (char *)realloc(s->ptr, new_len + 1);
    if (s->ptr == NULL)
    {
        fprintf(stderr, "realloc() failed\n");
        return 0;
        //exit(EXIT_FAILURE);
    }
    memcpy(s->ptr + s->len, ptr, size * nmemb);
    s->ptr[new_len] = '\0';
    s->len = new_len;

    return size * nmemb;
}

/*
bool convertSVGToPNG(std::string p_svgFilePath, std::string p_pngSaveFilePath)
{
    NSVGimage *image = NULL;
    NSVGrasterizer *rast = NULL;

    int w, h;

    image = nsvgParseFromFile(&p_svgFilePath[0u], "px", 96.0f);
    if (image == NULL)
    {
        return false;
    }

    w = (int)image->width;
    h = (int)image->height;

    rast = nsvgCreateRasterizer();
    if (rast == NULL)
    {
        return false;
    }

    unsigned char *img = (unsigned char *)malloc(w * h * 4);
    if (img == NULL)
    {
        return false;
    }

    nsvgRasterize(rast, image, 0, 0, 1, img, w, h, w * 4);

    stbi_write_png(&p_pngSaveFilePath[0u], w, h, 4, img, w * 4);\
    
    return true;
}
*/

void downloadTestFileV2()
{
    qrcodegen::QrCode qr0 = qrcodegen::QrCode::encodeText("https://www.linkedin.com/feed/", qrcodegen::QrCode::Ecc::MEDIUM);
    std::string svg = qr0.toSvgString(4);

    //netInit();
    //httpInit();

    SceCtrlData pad;
    vita2d_texture *image;
    vita2d_font *ttfFont;
    vita2d_font *vita2dFont[42];

    vita2d_init();
    vita2d_set_clear_color(RGBA8(0x40, 0x40, 0x40, 0xFF));

    ttfFont = vita2d_load_font_file("app0:assets/font/droidsans.ttf");

    for (int i = 0; i < 42; i++)
    {
        vita2dFont[i] = vita2d_load_font_file("app0:assets/font/whitney-book.ttf"); //droidsans.ttf
    }
    memset(&pad, 0, sizeof(pad));

    int httpResponse = 0;
    //Downloader downloader = Downloader();
    Files files = Files();
    std::string header = "fBAMHLqzG4YAAAAAAAAEWuaQzOkmBkf0ICv30R6XPkqKomPwLA7cGWl27CZdMMQP";
    std::string file1 = "file.sql";
    std::string file2 = "gabarito.xlsx";
    std::string file3 = "example5.txt";

    files.saveFileData("ux0:data/FBLK0001/svgfile.svg", svg);
    std::string pngFile = "ux0:data/FBLK0001/pngQRCode.png";
    bool converted = false;
    image = vita2d_load_PNG_file(&pngFile[0u]);

    //httpResponse = downloader.DropboxDownloadFile(header, file2, "ux0:data/FBLK0001/example6.txt");
    //httpResponse = downloader.DropboxUploadFile(header, file3, "ux0:data/FBLK0001/example4.txt");

    std::string tokenHeader = "Authorization: Bearer " + header;
    std::string fileNameHeader = "Dropbox-API-Arg: {\"path\": \"/" + file2 + "\"}";

    std::string fileData = "No Download Yet";

    while (1)
    {
        sceCtrlPeekBufferPositive(0, &pad, 1);

        if (pad.buttons & SCE_CTRL_START)
        {
            break;
        }

        if (pad.buttons & SCE_CTRL_CIRCLE)
        {
            if (httpResponse == 200)
            {
                //files.loadFileData("ux0:data/FBLK0001/example4.txt", &fileData);
                fileData = "Downloaded";
            }
            else
            {
                fileData = std::to_string(httpResponse);
            }
        }

        vita2d_start_drawing();
        vita2d_clear_screen();

        if (converted)
        {
            vita2d_draw_texture_scale(image, 0, 0,10,10);
        }

        std::string pngSize = std::to_string(pngFile.size());
        std::string response = std::to_string(httpResponse);
        printLog(vita2dFont[25], &tokenHeader[0u], 110);
        printLog(vita2dFont[25], &fileNameHeader[0u], 130);
        printLog(vita2dFont[25], "Response", 220);
        printLog(vita2dFont[25], &response[0u], 255);
        printLog(vita2dFont[25], &pngSize[0u], 270);

        vita2d_end_drawing();
        vita2d_swap_buffers();
    }

    /*
    * vita2d_fini() waits until the GPU has finished rendering,
    * then we can free the assets freely.
    */
    tools::LogToFile("-----END-----");
    vita2d_fini();
    vita2d_free_texture(image);
    vita2d_free_font(ttfFont);
}

void downloadTestFile()
{
    netInit();
    httpInit();

    tools::LogToFile("-----START-----\n");

    SceCtrlData pad;
    vita2d_texture *image;
    vita2d_font *ttfFont;
    vita2d_font *vita2dFont[42];

    vita2d_init();
    vita2d_set_clear_color(RGBA8(0x40, 0x40, 0x40, 0xFF));

    ttfFont = vita2d_load_font_file("app0:assets/font/droidsans.ttf");

    for (int i = 0; i < 42; i++)
    {
        vita2dFont[i] = vita2d_load_font_file("app0:assets/font/whitney-book.ttf"); //droidsans.ttf
    }
    memset(&pad, 0, sizeof(pad));

    int httpResponse = 0;

    Files files = Files();

    std::string url = "http://25.io/toau/audio/sample.txt";
    std::string fileName = "ux0:data/FBLK0001/example3.txt";

    tools::LogToFile("Open File\n");
    int fileID = files.openFileToSave(fileName);

    tools::LogToFile("Curl\n");
    CURL *curl_handle;

    curl_global_init(CURL_GLOBAL_ALL);

    /* init the curl session */
    curl_handle = curl_easy_init();

    /* set URL to get here */
    curl_easy_setopt(curl_handle, CURLOPT_URL, url.c_str());

    /* Switch on full protocol/debug output while testing */
    curl_easy_setopt(curl_handle, CURLOPT_VERBOSE, 1L);

    /* disable progress meter, set to 0L to enable and disable debug output */
    curl_easy_setopt(curl_handle, CURLOPT_NOPROGRESS, 1L);

    /* send all data to this function  */
    curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, write_data_to_disk);

    curl_easy_setopt(curl_handle, CURLOPT_SSL_VERIFYPEER, false);

    /* open the file */

    if (fileID >= 0)
    {
        tools::LogToFile("File OK\n");
        /* write the page body to this file handle */
        curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, &fileID);

        /* get it! */
        tools::LogToFile("Perform Curl\n");
        curl_easy_perform(curl_handle);
        tools::LogToFile("Curl info\n");
        curl_easy_getinfo(curl_handle, CURLINFO_RESPONSE_CODE, &httpResponse);
        tools::LogToFile(httpResponse + "\n");
        tools::LogToFile("End Curl\n");
    }

    tools::LogToFile("Close File\n");
    // close filedescriptor
    files.closeFile(fileID);

    // cleanup
    tools::LogToFile("Clean up\n");
    curl_easy_cleanup(curl_handle);
    curl_global_cleanup();

    std::string logData;
    files.loadFileTXTData("ux0:data/FBLK0001/log.txt", &logData);

    while (1)
    {
        sceCtrlPeekBufferPositive(0, &pad, 1);

        if (pad.buttons & SCE_CTRL_START)
            break;

        vita2d_start_drawing();
        vita2d_clear_screen();

        std::string response = std::to_string(httpResponse);
        printLog(vita2dFont[25], "Response", 220);
        printLog(vita2dFont[25], &response[0u], 255);
        printLog(vita2dFont[25], &logData[0u], 270);

        vita2d_end_drawing();
        vita2d_swap_buffers();
    }

    /*
    * vita2d_fini() waits until the GPU has finished rendering,
    * then we can free the assets freely.
    */
    tools::LogToFile("-----END-----");
    vita2d_fini();
    vita2d_free_texture(image);
    vita2d_free_font(ttfFont);
}

void jsonSample(){
    std::string jsonExample = "{\"login\": \"Teste\", \"password\": \"resultado\"}";

    json_t *root;
    json_error_t error;

    std::string jsonError = "No Error";
    json_t *data, *login, *password;

    root = json_loads(&jsonExample[0u], 0, &error);
    if (!root)
    {
        std::string jsonError = "Error loading json string";
    }

    std::string jsonSize = std::to_string(json_array_size(root));

    const char *message_text;

    if (!json_is_object(root))
    {
        jsonError = "Invalid Json data";
    }
    else
    {
        login = json_object_get(root, "login");
        password = json_object_get(root, "password");
    }
}

void readWriteFileToDisk()
{
    sceIoMkdir("ux0:data/FBLK0001", 0777);

    // open a file for writing only, create file if not exist, truncate
    SceUID uid = sceIoOpen("ux0:data/FBLK0001/example.txt", SCE_O_WRONLY | SCE_O_CREAT | SCE_O_TRUNC, 0777);

    // write to the file
    std::string text = "Teste de string";
    int bytes = sceIoWrite(uid, &text[0u], text.size()); // or strlen(data)

    // close the file
    sceIoClose(uid);

    // generate stats about the file
    SceIoStat info;
    sceIoGetstat("ux0:data/FBLK0001/example.txt", &info);

    // open a file for read only
    SceUID uid2 = sceIoOpen("ux0:data/FBLK0001/example.txt", SCE_O_RDONLY, 0);

    // allocate memory
    //char *data = new char[info.st_size + 1];
    std::string data;

    // read from file into data, append null terminator
    sceIoRead(uid2, &data[0u], info.st_size);
    data[info.st_size] = '\0'; // info.st_size - 1 would be index worthy
    sceIoClose(uid);

    SceCtrlData pad;
    vita2d_texture *image;
    vita2d_font *ttfFont;
    vita2d_font *vita2dFont[42];

    vita2d_init();
    vita2d_set_clear_color(RGBA8(0x40, 0x40, 0x40, 0xFF));

    ttfFont = vita2d_load_font_file("app0:assets/font/droidsans.ttf");

    for (int i = 0; i < 42; i++)
    {
        vita2dFont[i] = vita2d_load_font_file("app0:assets/font/whitney-book.ttf"); //droidsans.ttf
    }
    memset(&pad, 0, sizeof(pad));

    while (1)
    {
        sceCtrlPeekBufferPositive(0, &pad, 1);

        if (pad.buttons & SCE_CTRL_START)
            break;

        vita2d_start_drawing();
        vita2d_clear_screen();

        std::string bytesString = std::to_string(bytes);
        printLog(vita2dFont[25], &bytesString[0u], 320);
        printLog(vita2dFont[25], &data[0u], 355);

        vita2d_end_drawing();
        vita2d_swap_buffers();
    }

    /*
    * vita2d_fini() waits until the GPU has finished rendering,
    * then we can free the assets freely.
    */
    vita2d_fini();
    vita2d_free_texture(image);
    vita2d_free_font(ttfFont);
}

void readTestFile()
{
    SceCtrlData pad;
    vita2d_texture *image;
    vita2d_font *ttfFont;
    vita2d_font *vita2dFont[42];

    vita2d_init();
    vita2d_set_clear_color(RGBA8(0x40, 0x40, 0x40, 0xFF));

    ttfFont = vita2d_load_font_file("app0:assets/font/droidsans.ttf");

    for (int i = 0; i < 42; i++)
    {
        vita2dFont[i] = vita2d_load_font_file("app0:assets/font/whitney-book.ttf"); //droidsans.ttf
    }
    memset(&pad, 0, sizeof(pad));

    std::string fileData;
    Files files = Files();
    if (!files.loadFileTXTData("ux0:data/FBLK0001/example.txt", &fileData))
    {
        fileData = "ERROR";
    }

    while (1)
    {
        sceCtrlPeekBufferPositive(0, &pad, 1);

        if (pad.buttons & SCE_CTRL_START)
            break;

        vita2d_start_drawing();
        vita2d_clear_screen();

        printLog(vita2dFont[25], "Result:", 320);
        printLog(vita2dFont[25], &fileData[0u], 355);

        vita2d_end_drawing();
        vita2d_swap_buffers();
    }

    /*
    * vita2d_fini() waits until the GPU has finished rendering,
    * then we can free the assets freely.
    */
    vita2d_fini();
    vita2d_free_texture(image);
    vita2d_free_font(ttfFont);
}

void saveTestFile()
{
    SceCtrlData pad;
    vita2d_texture *image;
    vita2d_font *ttfFont;
    vita2d_font *vita2dFont[42];

    vita2d_init();
    vita2d_set_clear_color(RGBA8(0x40, 0x40, 0x40, 0xFF));

    ttfFont = vita2d_load_font_file("app0:assets/font/droidsans.ttf");

    for (int i = 0; i < 42; i++)
    {
        vita2dFont[i] = vita2d_load_font_file("app0:assets/font/whitney-book.ttf"); //droidsans.ttf
    }
    memset(&pad, 0, sizeof(pad));

    std::string newFileData = "NEW FILE HERE";
    Files files = Files();
    int bytes = files.saveFileData("ux0:data/FBLK0001/example2.txt", newFileData);

    std::string fileData;

    if (!files.loadFileTXTData("ux0:data/FBLK0001/example2.txt", &fileData))
    {
        fileData = "ERROR";
    }

    while (1)
    {
        sceCtrlPeekBufferPositive(0, &pad, 1);

        if (pad.buttons & SCE_CTRL_START)
            break;

        vita2d_start_drawing();
        vita2d_clear_screen();

        printLog(vita2dFont[25], "Result:", 320);
        printLog(vita2dFont[25], &fileData[0u], 355);

        vita2d_end_drawing();
        vita2d_swap_buffers();
    }

    /*
    * vita2d_fini() waits until the GPU has finished rendering,
    * then we can free the assets freely.
    */
    vita2d_fini();
    vita2d_free_texture(image);
    vita2d_free_font(ttfFont);
}
} // namespace mySamples