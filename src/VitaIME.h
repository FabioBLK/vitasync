#pragma once
#include <stdio.h>
#include <string.h>

#include <vita2d.h>
#include <psp2/display.h>
#include <psp2/apputil.h>
#include <psp2/ime_dialog.h>

static uint16_t ime_title_utf16[SCE_IME_DIALOG_MAX_TITLE_LENGTH];
static uint16_t ime_initial_text_utf16[SCE_IME_DIALOG_MAX_TEXT_LENGTH];
static uint16_t ime_input_text_utf16[SCE_IME_DIALOG_MAX_TEXT_LENGTH + 1];
static uint8_t ime_input_text_utf8[SCE_IME_DIALOG_MAX_TEXT_LENGTH + 1];

class VitaIME
{
private:
    const unsigned int IME_DIALOG_RESULT_NONE = 0;
    const unsigned int IME_DIALOG_RESULT_RUNNING = 1;
    const unsigned int IME_DIALOG_RESULT_FINISHED = 2;
    const unsigned int IME_DIALOG_RESULT_CANCELED = 3;

    void utf16_to_utf8(uint16_t *src, uint8_t *dst);
    void utf8_to_utf16(uint8_t *src, uint16_t *dst);
    void oslOskGetText(char *text);
    void initImeDialog(SceImeType type, char *title, char *initial_text, int max_text_length);
    int ime_dialog_type(SceImeType type, char *text, char *title, const char *def);

public:
    VitaIME();
    ~VitaIME();
    int ime_dialog_string(char *text, char *title, const char *def);
    int ime_dialog_number(char *text, char *title, const char *def);
};
