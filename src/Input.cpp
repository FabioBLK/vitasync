#include "Input.h"
#include <psp2/kernel/threadmgr.h>

Input::Input()
{
	sceCtrlPeekBufferPositive(0, &ctrl, 1);
	sceCtrlSetSamplingMode(SCE_CTRL_MODE_DIGITAL);
}

Input::~Input()
{
}

bool Input::GetKeyDown(SceCtrlButtons p_button)
{
	sceCtrlPeekBufferPositive(0, &ctrl, 1);

	if (ctrl.buttons & p_button)
	{
		//sceKernelDelayThread(1000000 / 7);
		return true;
	}

	return false;
}
