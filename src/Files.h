#pragma once
#include <string>
#include <vector>
#include <queue>
#include <psp2/types.h>

struct FilesData
{
    std::string name;
    bool isFolder;
    int type;
    int fileSize;
    SceDateTime mtime;
    bool selectedToTransfer;
};

class Files
{
private:
public:
    Files();
    ~Files();
    bool getDirData(std::string p_path, std::vector<FilesData> *p_data);
    bool getDirDataQueue(std::string p_path, std::queue<FilesData> *p_data);
    bool isDirValid(std::string p_path);
    bool createDir(std::string p_path);
    int getFileSize(std::string p_path);
    bool loadFileTXTData(std::string p_path, std::string *p_data);
    bool loadFileData(std::string p_path, FILE *p_fp, char *p_data, int p_fileSize);
    int saveFileData(std::string p_path, std::string p_data);
    int appendDataToFile(std::string p_path, std::string p_data);
    SceUID openFileToSave(std::string p_path);
    void closeFile(SceUID p_id);
    static size_t writeCurlDataCallBack(void *ptr, size_t size, size_t nmemb, FILE *stream);
    static size_t readCurlDataCallBack(char *buffer, size_t size, size_t nitems, void *instream);
};