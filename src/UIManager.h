#pragma once
#include <vita2d.h>
#include <string>
#include <vector>
#include "Files.h"

const static int MAX_FONT_SIZE = 42;
const static int DISPLAY_WIDTH = 960;
const static int DISPLAY_HEIGTH = 544;
const static int START_BOX_Y_POSITION = 30;
const static int END_BOX_Y_POSITION = 64;
const static int DIR_BOX_SIZE = 120;

class UIManager
{
private:
    int boxStartIndex = 0;
    vita2d_font *defaultFont;
    vita2d_font *sizableFont[MAX_FONT_SIZE];
    const unsigned int selectedTextColor = RGBA8(255, 0, 0, 255);
    const unsigned int regularTextColor = RGBA8(200, 200, 200, 255);

public:
    UIManager();
    ~UIManager();
    int StartUI();
    void FinishUI();
    void BeginFrame();
    void EndFrame();
    void DrawText(const std::string &p_text, const int p_xPosition, const int p_yPosition, const unsigned int p_color, const int p_size);
    void DrawDirList(const std::vector<FilesData> &p_list, int p_selected, bool p_fileSelectionEnabled, const unsigned int p_bgColor);
    void DrawDirBox(const int p_startPosY, const std::string &p_text, const bool p_isFolder,
                    const unsigned int &p_textColor, const bool p_fileSelectionEnabled,
                    const bool p_showSelectionMark);
    void DrawOptionBox(const int p_startPosY, const std::string &p_text, const unsigned int &p_textColor);
    vita2d_texture *LoadTexture(std::string p_path);
    void DrawTexture(vita2d_texture *p_texture, int p_xPos, int p_yPos, int p_scale);
    void UnloadTexture(vita2d_texture *p_textureRef);
    void DrawBGRectangle(int p_xPos, int p_yPos, int p_width, int p_height, const unsigned int p_color);
    void DrawDefaultBGRectangle(const unsigned int p_color);
    void DrawSyncInstructionsView(std::string &p_syncUrl, vita2d_texture *p_qrTexture,
                                  const unsigned int &p_bgColor, const unsigned int &p_textColor);
    void DrawAuthorizingView(const unsigned int &p_bgColor, const unsigned int &p_textColor, std::string &p_authCode,
                             std::string &p_authResult, std::string &p_downloadedStatus);
    void DrawFinishTransferView(const unsigned int &p_bgColor, const unsigned int &p_textColor,
                                std::string &p_downloadStatus, std::string &p_errorReport, std::string &p_currentLocalFolder);
    void DrawSelectTransferView(const unsigned int &p_bgColor, const unsigned int &p_textColor,
                                std::string &p_downloadStatus, std::string &p_currentLocalFolder);
    void DrawTransferProgressView(const unsigned int &p_bgColor, const unsigned int &p_textColor,
                                  std::string &p_downloadType, std::string &p_downloadStatus, std::string p_downloadCountStatus);
    void DrawConfigScreenView(std::string p_url, const unsigned int &p_bgColor, const unsigned int &p_textColor, vita2d_texture *p_textureRef,
                              std::vector<std::string> &p_optionsList, int p_selectPosition, int p_startBoxYPosition);
};