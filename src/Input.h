#pragma once

#include <psp2/ctrl.h>

class Input
{
public:
	Input();
	~Input();
	bool GetKeyDown(SceCtrlButtons p_button);
	int ime_dialog_string(char *text, char *title, const char *def);
	int ime_dialog_number(char *text, char *title, const char *def);

private:
	SceCtrlData ctrl;
};