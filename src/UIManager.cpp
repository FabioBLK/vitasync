#include "UIManager.h"
#include "tools.h"

UIManager::UIManager()
{
}

UIManager::~UIManager()
{
}

int UIManager::StartUI()
{
    int init = vita2d_init();
    if (init <= 0)
    {
        return 0;
    }

    vita2d_set_clear_color(RGBA8(192, 192, 192, 255));
    defaultFont = vita2d_load_font_file("app0:assets/font/droidsans.ttf");
    for (int i = 0; i < MAX_FONT_SIZE; i++)
    {
        sizableFont[i] = vita2d_load_font_file("app0:assets/font/whitney-book.ttf");
    }

    /*
    * Load the statically compiled image.png file.
    */
    //image = vita2d_load_PNG_buffer(&_binary_image_png_start);
    //bgImage = vita2d_load_PNG_file("app0:assets/image.png");

    return 1;
}

void UIManager::BeginFrame()
{
    vita2d_start_drawing();
    vita2d_clear_screen();
}

vita2d_texture *UIManager::LoadTexture(std::string p_path)
{
    return vita2d_load_PNG_file(&p_path[0u]);
}

void UIManager::UnloadTexture(vita2d_texture *p_textureRef)
{
    vita2d_free_texture(p_textureRef);
}

void UIManager::DrawTexture(vita2d_texture *p_texture, int p_xPos, int p_yPos, int p_scale)
{
    vita2d_draw_texture_scale(p_texture, p_xPos, p_yPos, p_scale, p_scale);
}

void UIManager::DrawText(const std::string &p_text, const int p_xPosition, const int p_yPosition, const unsigned int p_color, const int p_size)
{
    vita2d_font_draw_text(defaultFont, p_xPosition, p_yPosition, p_color, p_size, &p_text[0]);
}

void UIManager::DrawBGRectangle(int p_xPos, int p_yPos, int p_width, int p_height, unsigned int p_color)
{
    vita2d_draw_rectangle(p_xPos, p_yPos, p_width, p_height, p_color);
}

void UIManager::DrawDefaultBGRectangle(unsigned int p_color)
{
    int startXPosition = START_BOX_Y_POSITION;
    int endXPosition = START_BOX_Y_POSITION * 2;
    int maxY = DISPLAY_HEIGTH - END_BOX_Y_POSITION;

    vita2d_draw_rectangle(startXPosition, START_BOX_Y_POSITION, DISPLAY_WIDTH - endXPosition, maxY, p_color);
}

void UIManager::DrawDirList(const std::vector<FilesData> &p_list, int p_selected, bool p_fileSelectionEnabled, const unsigned int p_bgColor)
{
    int selected = p_selected;
    int startXPosition = START_BOX_Y_POSITION;
    int endXPosition = START_BOX_Y_POSITION * 2;
    int maxY = DISPLAY_HEIGTH - END_BOX_Y_POSITION;
    vita2d_draw_rectangle(startXPosition, START_BOX_Y_POSITION, DISPLAY_WIDTH - endXPosition, maxY, RGBA8(0, 20, 60, 100));

    int maxBoxes = ((maxY - START_BOX_Y_POSITION) / DIR_BOX_SIZE) + 1;
    maxBoxes = tools::Clamp(maxBoxes, 0, p_list.size());
    if (selected >= boxStartIndex + maxBoxes)
    {
        boxStartIndex++;
    }
    else if (selected < boxStartIndex)
    {
        boxStartIndex--;
    }

    boxStartIndex = tools::Clamp(boxStartIndex, 0, p_list.size());
    selected = (selected - boxStartIndex);

    for (int i = 0; i < maxBoxes; i++)
    {
        unsigned int color = selected == i ? selectedTextColor : regularTextColor;
        std::string text = p_list[boxStartIndex + i].name;
        int position = (i * DIR_BOX_SIZE) + START_BOX_Y_POSITION;
        DrawDirBox(position, text, p_list[boxStartIndex + i].isFolder, color,
                   (p_fileSelectionEnabled && !p_list[boxStartIndex + i].isFolder), p_list[boxStartIndex + i].selectedToTransfer);
    }
    //DrawText(std::to_string(p_selected) + " | startIndex = " + std::to_string(boxStartIndex));
}

void UIManager::DrawDirBox(const int p_startPosY, const std::string &p_text, const bool p_isFolder,
                           const unsigned int &p_textColor, const bool p_fileSelectionEnabled,
                           const bool p_showSelectionMark)
{
    int textPos = p_startPosY + 60;
    vita2d_font_draw_text(sizableFont[20], 200, textPos, p_textColor, 20, &p_text[0]);
    std::string fileType = (p_isFolder) ? "FOLDER" : "FILE";
    vita2d_font_draw_text(sizableFont[20], 600, textPos, p_textColor, 20, &fileType[0]);

    if (p_fileSelectionEnabled)
    {
        unsigned int fileSelectedColor = (p_showSelectionMark) ? RGBA8(0, 150, 0, 150) : RGBA8(0, 0, 0, 255);
        DrawBGRectangle(120, textPos, 35, 35, fileSelectedColor);
    }

    vita2d_draw_line(100, p_startPosY, DISPLAY_WIDTH - 100, p_startPosY, RGBA8(100, 100, 100, 255));
    vita2d_draw_line(100, p_startPosY + DIR_BOX_SIZE, DISPLAY_WIDTH - 100, p_startPosY + DIR_BOX_SIZE, RGBA8(100, 100, 100, 255));
}

void UIManager::DrawOptionBox(const int p_startPosY, const std::string &p_text, const unsigned int &p_textColor)
{
    int dirBoxSize = DIR_BOX_SIZE / 2;
    int textPos = p_startPosY + 30;
    DrawText(p_text, 200, textPos, p_textColor, 20);

    unsigned int bgColor = RGBA8(100, 100, 100, 100);

    vita2d_draw_rectangle(100, p_startPosY, DISPLAY_WIDTH - 200, dirBoxSize + 1, bgColor);
}

void UIManager::DrawSyncInstructionsView(std::string &p_syncUrl, vita2d_texture *p_qrTexture, const unsigned int &p_bgColor, const unsigned int &p_textColor)
{
    DrawDefaultBGRectangle(p_bgColor);
    DrawText("DROPBOX Account Sync", 50, 25, p_textColor, 20);
    DrawText("To link this app with your DROPBOX account, please open the url below and authorize the app: ", 50, 75, p_textColor, 20);
    DrawText(p_syncUrl, 50, 105, p_textColor, 20);
    DrawText("or scan the QRCode below using your phone", 50, 135, p_textColor, 20);
    DrawTexture(p_qrTexture, (DISPLAY_WIDTH / 2) - 125, 160, 4);
    DrawText("After authorizing the app in the browser, you'll receive a code.", 50, 440, p_textColor, 20);
    DrawText("Press \"START\" when you're ready to type the code", 50, 460, p_textColor, 20);
    DrawText("Press \"START\" to type the code from Dropbox", 50, DISPLAY_HEIGTH - 20, p_textColor, 14);
}

void UIManager::DrawAuthorizingView(const unsigned int &p_bgColor, const unsigned int &p_textColor, std::string &p_authCode, std::string &p_authResult, std::string &p_downloadedStatus)
{
    DrawDefaultBGRectangle(p_bgColor);
    DrawText("Authorizing Dropbox Account", 50, 25, p_textColor, 20);
    DrawText("Performing Dropbox authorization using key: ", 50, 75, p_textColor, 20);
    DrawText("Press \"X\" to type another authorization code", 50, DISPLAY_HEIGTH - 20, p_textColor, 14);
    DrawText(p_authCode, 50, 95, p_textColor, 20);
    DrawText(p_authResult, 50, 400, p_textColor, 14);
    DrawText(p_downloadedStatus, 50, 420, p_textColor, 14);
}

void UIManager::DrawSelectTransferView(const unsigned int &p_bgColor, const unsigned int &p_textColor, std::string &p_downloadStatus, std::string &p_currentLocalFolder)
{
    DrawDefaultBGRectangle(p_bgColor);
    DrawText("File Transfer", 50, 25, p_textColor, 20);
    DrawText("Press \"O\" button to start download", 50, 75, p_textColor, 20);
    DrawText("Press \"X\" button to start upload", 50, 95, p_textColor, 20);
    DrawText(p_downloadStatus, 50, 120, p_textColor, 20);
    DrawText("Current Selected Folder - " + p_currentLocalFolder, 50, DISPLAY_HEIGTH - 20, p_textColor, 14);
}

void UIManager::DrawFinishTransferView(const unsigned int &p_bgColor, const unsigned int &p_textColor, std::string &p_downloadStatus, std::string &p_errorReport, std::string &p_currentLocalFolder)
{
    DrawDefaultBGRectangle(p_bgColor);
    DrawText("File Transfer Finished", 50, 25, p_textColor, 20);
    DrawText("Press \"O\" button to start Start Over", 50, 75, p_textColor, 20);
    DrawText(p_downloadStatus, 50, 120, p_textColor, 20);
    if (!p_errorReport.empty())
    {
        DrawText("Error Log: " + p_errorReport, 50, 150, p_textColor, 20);
    }

    DrawText("Current Selected Folder - " + p_currentLocalFolder, 50, DISPLAY_HEIGTH - 20, p_textColor, 14);
}

void UIManager::DrawTransferProgressView(const unsigned int &p_bgColor, const unsigned int &p_textColor, std::string &p_downloadType, std::string &p_downloadStatus, std::string p_downloadCountStatus)
{
    DrawDefaultBGRectangle(p_bgColor);
    DrawText("File Transfer", 50, 25, p_textColor, 20);
    DrawText(p_downloadType + " IN PROGRESS", 50, 75, p_textColor, 20);
    DrawText(p_downloadStatus, 50, 120, p_textColor, 20);
    DrawText(p_downloadCountStatus, 50, 140, p_textColor, 20);
}

void UIManager::DrawConfigScreenView(std::string p_url, const unsigned int &p_bgColor, const unsigned int &p_textColor, vita2d_texture *p_textureRef,
                                     std::vector<std::string> &p_optionsList, int p_selectPosition, int p_startBoxYPosition)
{
    DrawDefaultBGRectangle(p_bgColor);
    DrawText("Initial Settings", 50, 25, p_textColor, 20);
    DrawText("Welcome to PS Vita Sync.", 50, 50, p_textColor, 20);
    DrawText("To start using this app you need to have a Dropbox App account created.", 50, 70, p_textColor, 20);
    DrawText("Once your Dropbox account is configured, fill in the AppKey and AppSecret.", 50, 90, p_textColor, 20);
    DrawText("For instructions on how to setup a Dropbox App Acccount, Please check the URL below.", 50, 110, p_textColor, 20);
    DrawText("Or open the QRCode with your smartphone", 50, 130, p_textColor, 20);
    DrawText(p_url, 50, 150, p_textColor, 20);
    DrawTexture(p_textureRef, (DISPLAY_WIDTH / 2) - 125, 180, 5);

    DrawText("Use the directional keys to change selection and press \"O\" to select.", 50, DISPLAY_HEIGTH - 22, p_textColor, 14);
    DrawText("Press \"START\" to save the settings and continue.", 50, DISPLAY_HEIGTH - 7, p_textColor, 14);

    for (int i = 0; i < p_optionsList.size(); i++)
    {
        unsigned int color = p_selectPosition == i ? selectedTextColor : regularTextColor;
        std::string text = p_optionsList[i];
        int boxPosition = (i * (DIR_BOX_SIZE / 2)) + p_startBoxYPosition;
        DrawOptionBox(boxPosition, text, color);
    }
}

void UIManager::FinishUI()
{
    vita2d_fini();
    vita2d_free_font(defaultFont);
    vita2d_free_font(*sizableFont);
}

void UIManager::EndFrame()
{
    vita2d_end_drawing();
    vita2d_swap_buffers();
}