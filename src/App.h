#include "UIManager.h"
#include "tools.h"
#include "Input.h"

const int BUTTON_SLEEP = 100000;
const int SLEEP_TIME = 10;

enum AppStatus
{
    Directory,
    RemoteDirectory,
    SyncInstructions,
    Authorizing,
    FileTransferDownload,
    FileTransferUpload,
    CheckStatus,
    SelectTransferAction,
    SelectLocalFiles,
    SelectRemoteFiles,
    ConfigScreen,
    Exit
};

class App
{
private:
    Input input;
    int buttonTime = 0;
    std::string rootPath = "ux0:/";
    std::string startPath = "ux0:/";
    std::string configFilePath = "ux0:data/psvitasync/config.json";
    std::string remoteFolder;
    unsigned int colorBlack = RGBA8(0, 0, 0, 255);
    unsigned int bgColorWhiteTransparent = RGBA8(255, 255, 255, 230);
    UIManager uiManager;
    AppStatus status = AppStatus::Directory;
    std::queue<FilesData> selectedLocalFiles;
    ConfigDataJSON configData;
    void SetStatus();
    void DirectoryView();
    void SyncInstructions();
    void FileTransferView();
    void AuthorizingView();
    void SelectTransferActionView();
    void ConfigScreenView();
    ConfigDataJSON CheckConfig();
    bool convertSVGToPNG(std::string p_svgFilePath, std::string p_pngSaveFilePath);

public:
    App();
    ~App();
    void StartApp();
};