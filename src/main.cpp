#include "mySamples.h"
#include "App.h"

#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <psp2/ctrl.h>
#include <psp2/kernel/processmgr.h>

int main()
{
	// Draw a sample scene on screen
	//mySamples::DrawSampleScreen();
	//mySamples::readTestFile();
	//mySamples::saveTestFile();
	//mySamples::downloadTestFile();
	//mySamples::downloadTestFileV2();
	
	App app;
	app.StartApp();

	sceKernelExitProcess(0);
	return 0;
}